angular.module('formController', [])

.controller('formController', function($scope, $http, Form) {

    $scope.formData = {};
    $scope.loading = false;
    $scope.errors = {};

    $scope.submitForm = function() {
        $scope.loading = true;
        $scope.errors = {};
        $scope.success = false;

        Form.save($scope.formData)
            .success(function(data) {
                $scope.success = true;
                $scope.loading= false;
                $scope.formData = {};
                $('html,body').animate({ scrollTop: 0 }, 400);
            })
            .error(function(data) {
                $scope.errors = data;
                $scope.errors.notification = true;
                $scope.loading = false;
                $('html,body').animate({ scrollTop: 0 }, 400);
            });
    };

});