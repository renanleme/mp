angular.module('adminController', [])

.controller('adminController', function($scope, $http, Form) {

    $scope.main = {
        page: 1,
        range: [],
        currentPage:  1,
        totalPages: 0
    };

    $scope.loading = true;
    $scope.pagination = true;

   
    Form.get($scope.main.page)
        .success(function(data) {
            $scope.paginate(data);
        });

    $scope.getPages = function(page) {
        Form.get(page)
        .success(function(data) {
            $scope.paginate(data);
        });
    }

    $scope.paginate = function(data) {
        $scope.pagination = true;
        $scope.forms = data.data;
        $scope.loading = false;
        $scope.main.totalPages = data.last_page;
        $scope.main.currentPage = data.current_page;
        $scope.main.pages = data.pages;

        var pages = [];
        for(var i=1;i<=data.last_page;i++) {          
            pages.push(i);
        }
        $scope.main.range = pages; 
    }

    $scope.$watch('query', function (newValue, oldValue) {
      if (newValue != oldValue) $scope.formSearch();
    }, true);

    $scope.formSearch = function() {
        $scope.loading = true;
        $scope.pagination = false;

        if($scope.query === "") {
            Form.get($scope.main.page)
            .success(function(data) {
                $scope.paginate(data);
            });
        } else {
        Form.search($scope.query)
            .success(function(data) {   
                $scope.forms = data;
                $scope.loading = false;

            })
            .error(function(data) {
                console.log(data);
            });
        }
    };

    $scope.deleteForm = function(id) {
        if (confirm("Tem certeza que deseja remover?")) {
            $scope.loading = true; 

            Form.destroy(id)
                .success(function(data) {
                    Form.get($scope.main.currentPage)
                        .success(function(response) {
                            $scope.paginate(response);
                            $scope.loading = false;
                        });

                });
        }
    };

    $scope.editForm = function(data, id) {
        angular.extend(data, {id: id});
        return $http.put('api/forms/' + id, data);

    };

    $scope.nextPage = function() {
        if ($scope.main.page < $scope.main.pages) {
            $scope.main.page++;
            $scope.loadPage();
        }
    };
    
    $scope.previousPage = function() {
        if ($scope.main.page > 1) {
            $scope.main.page--;
            $scope.loadPage();
        }
    };
})



.directive('postsPagination', function(){  
   return{
      restrict: 'E',
      template: '<ul class="pagination">'+
        '<li ng-class="{disabled: main.currentPage == 1}"><a href="javascript:void(0)" ng-click="getPages(1)">&laquo;</a></li>'+
        '<li ng-class="{disabled: main.currentPage == 1}"><a href="javascript:void(0)" ng-click="getPages(main.currentPage-1)">&lsaquo; Anterior</a></li>'+
        '<li ng-repeat="i in main.range" ng-class="{active : main.currentPage == i}">'+
            '<a href="javascript:void(0)" ng-click="getPages(i)">{{i}}</a>'+
        '</li>'+
        '<li ng-class="{disabled: main.currentPage == main.totalPages}"><a href="javascript:void(0)" ng-click="getPages(main.currentPage+1)">Próxima &rsaquo;</a></li>'+
        '<li ng-class="{disabled: main.currentPage == main.totalPages}"><a href="javascript:void(0)" ng-click="getPages(main.totalPages)">&raquo;</a></li>'+
      '</ul>'
   };
});

