angular.module('formService', [])
.factory('Form', function($http) {
    return {
        get : function(page) {
            return $http.get('api/forms?page=' + page);
        },
        save : function(formData) {
            return $http({
                method: 'POST',
                url: 'api/forms',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(formData)
            });
        },
        destroy : function(id) {
            return $http.delete('api/forms/' + id);
        },
        search: function (query) {
            return $http.get('api/forms/search?q=' + query);
        }
    }
});
