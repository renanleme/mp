<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Form;
use Response, Input, HTML, Mail, DB;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Form::paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'nome' => 'required|max:255',
                'email' => 'required|email|unique:forms',
            ]);

            $userForm = Form::create([
                'nome'        => $request->input('nome'),
                'email'       => $request->input('email'),
                'html'        => $request->input('html'),
                'css'         => $request->input('css'),
                'javascript'  => $request->input('javascript'),
                'python'      => $request->input('python'),
                'django'      => $request->input('django'),
                'ios'         => $request->input('ios'),
                'android'     => $request->input('android')
            ]);

            $html       = $userForm->html;
            $css        = $userForm->css;
            $javascript = $userForm->javascript;
            $python     = $userForm->python;
            $django     = $userForm->django;
            $ios        = $userForm->ios;
            $android    = $userForm->android;

            $generic = true;

            if($html >= 7 && $css >= 7 && $javascript >=7) {
                $this->sendMail($userForm, 'Front-End');
                $generic = false;
            }
            if($python >= 7 && $django >= 7) {
                $this->sendMail($userForm, 'Back-End');   
                $generic = false;
            }
            if($ios >= 7 || $android >= 7) {
                $this->sendMail($userForm, 'Mobile');   
                $generic = false;
            }
            if($generic) {
                $this->sendMail($userForm, '');   
            }
           
        } catch(Exception $e) {
            DB::rollBack();
            return Response::json(['error' => 'Falha ao cadastrar!'], HttpResponse::HTTP_CONFLICT);
        }

        DB::commit();

        return Response::json(array('success' => true));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

        $form = Form::find($id);
        $form->nome         = $request->input('nome');
        $form->email        = $request->input('email');
        $form->html         = $request->input('html');
        $form->css          = $request->input('css');
        $form->javascript   = $request->input('javascript');
        $form->python       = $request->input('python');
        $form->django       = $request->input('django');
        $form->ios          = $request->input('ios');
        $form->android      = $request->input('android');

        $form->save();

        } catch (Exception $e) {
            return Response::json(['error' => 'Falha ao editar o registro!'], HttpResponse::HTTP_CONFLICT);

        }

        return Response::json(array('success' => true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        try {

            Form::destroy($id);

        } catch (Exception $e) {

            return Response::json(['error' => 'Falha ao apagar o registro!'], HttpResponse::HTTP_CONFLICT);

        }

        return Response::json(array('success' => true));
    }

    protected function sendMail($user, $template = '')
    {
        
        $data = [
            'nome'           => $user->nome,
            'email'          => $user->email,
            'template'       => $template,
            'title'          => 'Obrigado por se candidatar!'
        ];

        Mail::queue('emails.confirmation', $data, function ($message) use ($user)
        {
            $message->to($user->email)
                    ->sender('noreply@meuspedidos.com.br')
                    ->subject('Obrigado por se candidatar!')
                    ->replyTo('contato@meuspedidos.com.br', 'contato@meuspedidos.com.br');
        });

    }

    public function search(Request $request) {
       $q = $request->input('q');

       $forms = Form::Where('nome', 'LIKE', '%'. $q.'%')
              ->orWhere('email', 'LIKE', '%'. $q.'%')
              ->get();

       return Response::json($forms);
    }
}
