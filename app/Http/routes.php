<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api'], function() {
	Route::resource('forms', 'FormController', ['only' => ['index', 'store', 'destroy', 'update']]);
	Route::get('forms/search', 'FormController@search');
});

Route::get('/admin', function() {
	return File::get(base_path() . '/js/views/admin.html');
});

Route::get('/', function() {
	return File::get(base_path() . '/js/views/index.html');
});


