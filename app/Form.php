<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = array('nome', 'email', 'html', 'css', 'javascript', 'python', 'django', 'ios', 'android');
}

