@extends('_layouts.email')
@section('main')

<tbody>
    <tr>
        <td class="content-block">
            <h3 style="font-size:20px;">Olá, {{ $nome }}</h3><h4 style="font-size:16px;"></h4>
        </td>
    </tr>
    <tr>
        <td class="content-block" style="padding: 0 0 15px;">Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador {{ $template }} entraremos em contato</td>
    </tr>
</tbody>

@stop