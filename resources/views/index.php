<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cadastro de Candidato - Meus Pedidos</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.6/angular.min.js"></script> 
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.6/angular-route.js"></script> 
    <script src="js/app.js"></script> 
    <script src="js/controllers/formController.js"></script> 
    <script src="js/controllers/adminController.js"></script> 
    <script src="js/services/formService.js"></script> 
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <style>
    	body { padding-top: 60px; }
		#loading {
		    position:   fixed;
		    z-index:    1000;
		    top:        0;
		    left:       0;
		    height:     100%;
		    width:      100%;
		    background: rgba( 255, 255, 255, .8 ) 
		                url('images/loading.gif') 
		                50% 50% 
		                no-repeat;
		}

		/* When the body has the loading class, we turn
		   the scrollbar off with overflow:hidden */
		body.loading {
		    overflow: hidden;   
		}
    </style>

  </head>
	<body ng-app="mpApp">
		<div class="container-fluid">
			<div>
				<div class="row">
					<div class="col-md-12">
						<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
							<div class="container">
								<div class="navbar-header">
									<a class="navbar-brand" href="#">Meus Pedidos</a>
								</div>
								
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav">
										<li class="active">
											<a href="#">Formulário</a>
										</li>
									</ul>

									<ul class="nav navbar-nav navbar-right">
										<li class="dropdown">
											 <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i><strong class="caret"></strong></a>
											<ul class="dropdown-menu">
												<li>
													<a ng-href="admin">Admin</a>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</nav>
						<ng-view></ng-view>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>