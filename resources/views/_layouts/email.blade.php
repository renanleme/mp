<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ $title }}</title>

    <style type="text/css">
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 640px) {
            h1, h2, h3, h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content, .content-wrap {
                padding: 10px !important;
            }

        }
    </style>
</head>
<body style="-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100% !important;height:100%;line-height:1.6;background-color:#f6f6f6;">
    <table class="body-wrap" style="background-color:#f6f6f6;width:100%;">
        <tr>
            <td class="container" width="600" style="vertical-align:top;display: block !important;max-width:600px !important;margin:0 auto !important;clear:both !important;">
                <div class="content" style="max-width:600px;margin:0 auto;display:block;padding:20px;">
                    <div class="header" style="width:100%;margin-bottom:20px;">
                        <table width="100%">
                            <tr>
                                <td class="aligncenter content-block" style="vertical-align:top;text-align:center;padding:0 0 20px;">
                                    <a href="http://www.meuspedidos.com.br" target="_blank"><img class="img-responsive" src="{{ asset('images/logo.gif') }}" style="max-width:100%;text-decoration:none;color:transparent;"/></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table style="background:#fff;border:1px solid #e9e9e9;border-radius:3px;" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="content-wrap" style="vertical-align:top;padding:20px;">
                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 14px;">

                                    @yield('main')
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div class="footer" style="width: 100%;clear:both;color:#999;padding:20px 0;">
                        <table width="100%">
                            <tr>
                                <td class="aligncenter content-block" style="vertical-align:top;text-align:center;padding: 0 0 20px;"><a href="http://www.meuspedidos.com.br" target="_blank" style="color:#999;font-size:12px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;">Meus Pedidos</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>